United Nations Peacekeeping Operations
=======================

http://drupal.org/sandbox/kravetz/2452999

What Is This?
-------------

This a content module that creates a simple page getting information from United
Nations XML data.
Currently, the page shows information about peacekeeping operations, and the
content can be accessed in the node /united_nations_peacekeeping_operations.

How To Install The Modules
--------------------------

1. Install United Nations Peacekeeping Operations (unpacking it to your Drupal
/sites/all/modules directory if you're installing by hand, for example).

2. Enable the module in Admin menu > Site building > Modules.

4. Done!
